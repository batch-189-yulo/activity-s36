const express = require('express')
const TaskController = require('../controllers/TaskControllers.js')
const router = express.Router()

// Create Tasks
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((task) => response.send(task))
})

// Locate a Specific Task
router.get('/:id', (request, response) => {
	TaskController.getTask(request.params.id).then((tasks) => response.send(tasks))
})

// Updating a Task

router.put('/:id/complete', (request, response) => {
TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask))
})

module.exports = router