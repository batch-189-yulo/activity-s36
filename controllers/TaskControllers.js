const Task = require('../models/Task.js')

// CREATE task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error
		}
		return 'Task created successfully!'
	})
}

// Locate a specific Task
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((tasks, error) => {
		if(error){
			return error
		}

			return tasks
		})
	}

// Updating a Task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}
		result.status = newContent.status

		return result.save().then((updateTask, error) => {
			if(error){
				return error
			}

			return updateTask
		})
	})
}